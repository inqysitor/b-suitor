INPUTPATH="../../data" #in relation to /aa123123/build/
OUTPUTPATH="../../ref-results" #in relation to /aa123123/build/
LIBPATH="../lib/" #in relation to /aa123123/
PROGRAM="./adorate" #in relation to /aa123123/build/
SOURCEPATH=$(find . -type d -print | grep -E './[a-z]{2}[0-9]{6}$') #in relation to /

echo "Your source path is $SOURCEPATH"
timing=false
singletest=false
threads=4

POSITIONAL=()
while [[ $# -gt 0 ]]
do
	key="$1"
	case $key in
		-m|--measure)
		timing=true
		shift
		;;
		-t|--test)
		singletest=true
		testname="$2"
		shift
		shift
		;;
		-n|--threads)
		threads="$2"
		shift
		shift
		;;
		*)
		POSITIONAL+=("$1")
		shift
		;;
	esac
done
set -- "${POSITIONAL[@]}"

echo $timing
echo $singletest

cd ${SOURCEPATH}
cp -R ${LIBPATH}. .
if [ ! -d "build" ]; then
	mkdir build
fi
cd build

for i in $INPUTPATH/*.in; do
	i_cut=$(basename "${i}")
	i_cut="${i_cut/.in/}"
	sed -e '/'$i_cut'/ s/^#//' -i ../CMakeLists.txt
done

if [ "$singletest" = true ]; then
	for i in $INPUTPATH/*.in; do
		i_cut=$(basename "${i}")
		i_cut="${i_cut/.in/}"
		if [ "$testname" != "$i_cut" ]; then
			sed -e '/'$i_cut'/ s/^#*/#/' -i ../CMakeLists.txt
		fi
	done
fi

cmake ..
make
rm "../blimit-"*.cpp

if [ "$singletest" = false ]; then
	out_incorrect=0
	out_total=0
	for i in $INPUTPATH/*.in; do
		((out_total++))
		i_cut=$(basename "${i}")

		out_file="${i_cut/.in/.out}"

		cur_program="${PROGRAM}_${i_cut}"
		cur_program="${cur_program/.in/}"

		blimit=$(awk '/^# blimit/ {print $3}' $i)

		if [ "$timing" = false ]; then
			output=$($cur_program $threads $i $blimit | diff - $OUTPUTPATH/$out_file)
			if [ -n "$output" ]; then
				echo "Test $i_cut failed."
				echo "$output"
				((out_incorrect++))
			else
				echo "Test $i_cut ok"
			fi
		else
			echo "${i_cut}:"
			output=$(time $cur_program $threads $i $blimit)
		fi
		
	done
	echo "$(($out_total-$out_incorrect))/$out_total tests passed."
else
	i_cut="${testname}.in"
	i="${INPUTPATH}/${i_cut}"

	out_file="${i_cut/.in/.out}"

	cur_program="${PROGRAM}_${i_cut}"
	cur_program="${cur_program/.in/}"

	blimit=$(awk '/^# blimit/ {print $3}' $i)
	if [ "$timing" = false ]; then
		output=$($cur_program $threads $i $blimit | diff - $OUTPUTPATH/$out_file)
		if [ -n "$output" ]; then
			echo "Test $i_cut failed."
			echo "$output"
		else
			echo "Test $i_cut ok"
		fi
	else
		echo "${i_cut}:"
		output=$(time $cur_program $threads $i $blimit)
	fi
fi